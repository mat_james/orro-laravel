# Getting Started
1) Link images, from the html/public folder run "ln -s ../storage/app/public ./storage" (We can't use storage link as we need a relative path)
2) From this directory run "vagrant up" in terminal.
3) Run "vagrant reload --provision" ()The user for storage is created after the initial file sync so the storage folder isn't shared with correct user)
4) Wait for the machine to load (first time this ay take a while as the box needs to download).
5) Add "vm.orrbikes.com" to your hosts file with the same IP as the "orrobikesmachine.vm" which has been added for you.
6) Copy the ".env.example" file and add the database connection details and app url: 
```
APP_URL=http://vm.orrobikes.com

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=orrobikes
DB_USERNAME=orrobikes
DB_PASSWORD=orrobikes
```
7) ssh into the machine using "vagrant ssh"
8) "cd /var/www/html"
9) Run "composer install"
10) Run "php artisan key:generate"
11) Run "php artisan migrate"
12) Run "php artisan appshell:super" (generate your user)
13) In a web browser go to "vm.orrobikes.com" (Check it's working)
14) To connect to mysql from local you can connect using Sequel Pro using the following details in the "SSH tab"
```
MySQL Host: 127.0.0.1
Username: root
Password: 123
SSH Host: orrobikesmachine.vm
SSH User: vagrant
SSH key: ~/.vagrant.d/insecure_private_key
```
